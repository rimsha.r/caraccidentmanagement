CREATE TABLE accident_user
(
    id              INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    accident_id INT UNSIGNED NOT NULL,
	user_id		INT UNSIGNED NOT NULL,
	role_id		INT UNSIGNED NOT NULL,

	FOREIGN KEY (accident_id)
    		REFERENCES accidents (id)
    		ON DELETE RESTRICT,
   	FOREIGN KEY (user_id)
    		REFERENCES users (id)
    		ON DELETE RESTRICT,
    FOREIGN KEY (role_id)
    		REFERENCES roles (id)
    		ON DELETE RESTRICT

);

INSERT INTO accident_user (accident_id, user_id, role_id)
VALUES
(1, 1, 1),
(1, 2, 2),
(1, 3, 3),
(2, 3, 2),
(2, 6, 1),
(3, 4, 2),
(3, 3, 1),
(4, 3, 1),
(4, 4, 2),
(2, 1, 3),
(2, 2, 3),
(3, 5, 3);