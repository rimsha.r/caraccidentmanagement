CREATE TABLE accidents
(
    id              INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	time            DATETIME NOT NULL,
	locations_id     INT UNSIGNED NOT NULL,
	submitted       BIT DEFAULT 0,

	FOREIGN KEY (locations_id)
    		REFERENCES locations (id)
    		ON DELETE RESTRICT

);

INSERT INTO accidents (time, locations_id, submitted)
VALUES (TIME("12:12:12"), 1, 0),
(TIME("08:15:00"), 2, 0),
(TIME("23:04:03"), 3, 0),
(TIME("17:00:35"), 4, 1),
(TIME("21:56:01"), 5, 0);