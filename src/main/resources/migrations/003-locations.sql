CREATE TABLE locations
(
	id         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	city       VARCHAR(32) NOT NULL,
	street    VARCHAR(32) NOT NULL,
	home_number VARCHAR(32) NOT NULL
);

INSERT INTO locations (city, street, home_number)
VALUES ("Tallinn", "Oola Strasse", "33"),
("Riga", "MussoliniStrasse", "34"),
("Wuhan", "Crown street", "13"),
("Riga", "Monikas iela", "314"),
("Valmiera", "Lenina iela", "18");