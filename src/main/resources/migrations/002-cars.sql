CREATE TABLE cars
(
	id            INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	plate_nr      VARCHAR(32)   NOT NULL,
	car_brand     VARCHAR(32)   NOT NULL

);

INSERT INTO cars  (plate_nr, car_brand)
VALUES ("AF-2323", "Mazda"),
("KVZ-000", "Audi"),
("FG-6543", "BMW"),
("SQL-404", "Mercedes"),
("DTF-9999", "Lada"),
("SPICAIS", "Bentley");
