CREATE TABLE roles (
id int UNSIGNED NOT NULL,
role_type varchar(255),
PRIMARY KEY (id)
);


INSERT INTO roles (id, role_type)
VALUES (1, "offender"), (2, "victim"), (3, "witness");