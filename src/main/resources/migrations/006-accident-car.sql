CREATE TABLE accident_car
 (
  id int unsigned AUTO_INCREMENT PRIMARY KEY,
  approved BIT DEFAULT 0,
car_id INT UNSIGNED NOT NULL,
accident_id INT UNSIGNED NOT NULL,
driver_id INT UNSIGNED NOT NULL,

FOREIGN KEY (car_id)
    		REFERENCES cars (id)
    		ON DELETE RESTRICT,
FOREIGN KEY (accident_id)
    		REFERENCES accidents (id)
    		ON DELETE RESTRICT,
FOREIGN KEY (driver_id)
    		REFERENCES users (id)
    		ON DELETE RESTRICT
);

INSERT INTO accident_car (approved, car_id, accident_id, driver_id)
VALUES (0, 1, 1, 1),
       (0, 2, 1, 2),
       (0, 3, 2, 3),
       (1, 4, 4, 4),
       (1, 5, 4, 5),
       (0, 6, 2, 6),
       (1, 3, 3, 3),
       (0, 2, 3, 4);