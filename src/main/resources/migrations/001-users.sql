CREATE TABLE users (
    id int UNSIGNED AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    email varchar (255),
    PRIMARY KEY (id)
);

INSERT INTO users (first_name, last_name, email)
VALUES ("Ola", "Hasslund", "ola@ms.lv"),
("Jokke", "Jokke", "jokee.ko@aaa.com"),
("Izvagars", "Obana", "obana@rambler.ru"),
("Timmy", "Schmit", "tbone@protonmail.org"),
("Test", "User", "tfest@test.ng"),
("Anna", "Panna", "panpan@anna.lv");
