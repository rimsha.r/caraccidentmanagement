package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.cars.Cars;
import com.sda.latnikovd.springbootapp.modules.cars.CarsService;
import com.sda.latnikovd.springbootapp.modules.locations.Location;
import com.sda.latnikovd.springbootapp.modules.locations.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/locations")
public class LocationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    private LocationService locationService;

    @GetMapping
    public List<Location> findAll() {
        return locationService.findAll();
    }

    @PutMapping
    public Location create(@RequestBody Location location) {
        return locationService.save(location);
    }

    @PatchMapping
    public Location update(@RequestBody Location location) {
        return locationService.save(location);
    }
}