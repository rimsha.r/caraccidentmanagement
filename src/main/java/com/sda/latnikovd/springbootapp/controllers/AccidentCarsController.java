package com.sda.latnikovd.springbootapp.controllers;

import java.util.List;

import com.sda.latnikovd.springbootapp.modules.accidentcars.AccidentCars;
import com.sda.latnikovd.springbootapp.modules.accidentcars.AccidentCarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/accidentcars")
public class AccidentCarsController {

    @Autowired
    private AccidentCarsService accidentCarsService;

    @GetMapping
    public List<AccidentCars> findAll() {
        return accidentCarsService.findAll();
    }

    @PutMapping
    public AccidentCars create(@RequestBody AccidentCars accidentCars) {
        return accidentCarsService.save(accidentCars);
    }


    @PatchMapping("/{accidentCarId}")
    public void approveCar (@PathVariable long accidentCarId){
        accidentCarsService.approve(accidentCarId);
    }

    @DeleteMapping("/{accidentId}/{carId}")
    public void delete(@PathVariable long accidentId, @PathVariable long carId) {
        accidentCarsService.delete(accidentId, carId);
    }
}
