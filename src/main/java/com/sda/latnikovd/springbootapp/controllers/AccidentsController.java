package com.sda.latnikovd.springbootapp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sda.latnikovd.springbootapp.modules.accidents.Accidents;
import com.sda.latnikovd.springbootapp.modules.accidents.AccidentsService;

@RestController
@RequestMapping("/accidents")
public class AccidentsController {

    @Autowired
    private AccidentsService accidentsService;

    @GetMapping
    public List<Accidents> findAll() {
        return accidentsService.findAll();
    }

    @PutMapping
    public Accidents create(@RequestBody Accidents accidents) {
        return accidentsService.save(accidents);
    }

    @PatchMapping
    public Accidents update(@RequestBody Accidents accidents) {
        return accidentsService.save(accidents);
    }
}
