package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.accidentuser.AccidentUser;
import com.sda.latnikovd.springbootapp.modules.accidentuser.AccidentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accidentusers")
public class AccidentUserController {

    @Autowired
    private AccidentUserService accidentUserService;

    @GetMapping
    public List<AccidentUser> findAll() {
        return accidentUserService.findAll();
    }

    @PutMapping
    public AccidentUser create(@RequestBody AccidentUser accidentUser) {
        return accidentUserService.save(accidentUser);
    }

    @PatchMapping
    public AccidentUser update(@RequestBody AccidentUser accidentUser) {
        return accidentUserService.save(accidentUser);
    }

    @GetMapping("/users/{userId}")
    public List<AccidentUser> getUser(@PathVariable long userId) {
        return accidentUserService.findAllByUserId(userId);
    }

    @GetMapping("/accidents/{accidentId}")
    public List<AccidentUser> getAccident(@PathVariable long accidentId) {
        return accidentUserService.findAllByAccidentId(accidentId);
    }


    @DeleteMapping("/{accidentId}/{userId}")
    public void delete(@PathVariable long accidentId, @PathVariable long userId) {
        accidentUserService.delete(accidentId, userId);
    }
}
