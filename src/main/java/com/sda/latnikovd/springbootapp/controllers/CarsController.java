package com.sda.latnikovd.springbootapp.controllers;


import com.sda.latnikovd.springbootapp.modules.cars.Cars;
import com.sda.latnikovd.springbootapp.modules.cars.CarsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarsController.class);

    @Autowired
    private CarsService carsService;

    @GetMapping
    public List<Cars> findAll() {
        return carsService.findAll();
    }

    @PutMapping
    public Cars create(@RequestBody Cars cars) {
        return carsService.save(cars);
    }

    @PatchMapping
    public Cars update(@RequestBody Cars cars) {
        return carsService.save(cars);
    }
}

