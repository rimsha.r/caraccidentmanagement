package com.sda.latnikovd.springbootapp.controllers;

import java.util.List;

import com.sda.latnikovd.springbootapp.modules.roles.Roles;
import com.sda.latnikovd.springbootapp.modules.roles.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/roles")
public class RolesController {

    @Autowired
    private RolesService rolesService;

    @GetMapping
    public List<Roles> findAll() {
        return rolesService.findAll();
    }

}
