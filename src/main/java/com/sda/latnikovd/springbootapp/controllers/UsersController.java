package com.sda.latnikovd.springbootapp.controllers;

import java.util.List;

import com.sda.latnikovd.springbootapp.modules.users.User;
import com.sda.latnikovd.springbootapp.modules.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    @PutMapping
    public User create(@RequestBody User user) {
        return userService.save(user);
    }

    @PatchMapping
    public User update(@RequestBody User user) {
        return userService.update(user);
    }

    @DeleteMapping ("/{userId}")
    public void delete(@PathVariable long userId) {
        userService.delete(userId);
    }
}
