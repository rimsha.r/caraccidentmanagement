package com.sda.latnikovd.springbootapp.modules.accidentuser;

import java.util.List;

import com.sda.latnikovd.springbootapp.modules.accidents.Accidents;
import com.sda.latnikovd.springbootapp.modules.accidents.AccidentsRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccidentUserService {

    @Autowired
    private AccidentUserRepository accidentUserRepository;

    @Autowired
    private AccidentsRepository accidentsRepository;

    @Transactional(readOnly = true)
    public List<AccidentUser> findAll() {
        return accidentUserRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public AccidentUser save(final AccidentUser accidentUser) {

        Long accidentUserId = accidentUser.getId();
        Validate.notNull(accidentUser.getAccidentId(), "accidentId is blank for this accidentUser '%s'", accidentUser);
        Validate.notNull(accidentUser.getUserId(), "userId is blank for this accidentUser '%s'", accidentUser);
        Validate.notNull(accidentUser.getRoleId(), "roleId is blank for this accidentUser '%s'", accidentUser);


        Validate.isTrue(!accidentUserRepository.findByAccidentIdAndUserId(accidentUser.getAccidentId(), accidentUser.getUserId()).isPresent(),
                "User already added to this accident!");


        Validate.isTrue(accidentUser.getId() == null, "ID cannot be defined in body!");


        return accidentUserRepository.save(accidentUser);
    }

    @Transactional(readOnly = true)
    public List<AccidentUser> findAllByUserId(final long userId) {
        return accidentUserRepository.findAllByUserId(userId);
    }

    @Transactional(readOnly = true)
    public List<AccidentUser> findAllByAccidentId(final long accidentId) {
        return accidentUserRepository.findAllByAccidentId(accidentId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(long accidentId, long userId) {

        AccidentUser accidentUser = accidentUserRepository.findByAccidentIdAndUserId(accidentId, userId).orElseThrow(()
        -> new RuntimeException("No such user found for this accident!"));

        Accidents foundAccident = accidentsRepository.findById(accidentId).orElse(null);
        Validate.notNull(foundAccident, "No such accident found!");

        Validate.isTrue(!foundAccident.isSubmitted(), "Accident is already submitted!");

        accidentUserRepository.deleteById(accidentUser.getId());
    }

}
