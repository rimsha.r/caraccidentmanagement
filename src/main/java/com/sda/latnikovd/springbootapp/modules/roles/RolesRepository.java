package com.sda.latnikovd.springbootapp.modules.roles;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// please note that repository class is package private to minimize it's visibility
// only services should use corresponding repository classes!
@Repository
interface RolesRepository extends JpaRepository<Roles, Long> {

}

