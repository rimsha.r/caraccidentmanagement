package com.sda.latnikovd.springbootapp.modules.locations;

import java.util.List;

import com.sda.latnikovd.springbootapp.modules.users.User;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

// here we will put all the logic for authors model
@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Transactional(readOnly = true)
    public List<Location> findAll() {
        return locationRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Location save(final Location location) {

        Validate.notNull(location.getCity(), "City is blank for location '%s'", location);
        Validate.notNull(location.getStreet(), "Street is blank for location '%s'", location);
        Validate.notNull(location.getHomeNumber(), "Home number is blank for location '%s'", location);

        Validate.isTrue(location.getId() == null, "ID cannot be defined in body!");

        return locationRepository.save(location);
    }
}
