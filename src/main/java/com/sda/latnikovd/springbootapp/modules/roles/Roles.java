package com.sda.latnikovd.springbootapp.modules.roles;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Roles {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String roleType;

    public enum RoleType {

        OFFENDER(1L, "offender"),
        VICTIM(2L, "victim"),
        WITNESS(3L, "witness");

        private Long id;
        private String roleType;

        RoleType(Long id, String roleType) {
            this.id = id;
            this.roleType = roleType;
        }

        public Long getId() {
            return id;
        }
    }

     public Roles() {
    }

    public Roles(final String roleType) {
        this.roleType = roleType;

    }

    public Long getId() {
        return id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(final String roleType) {
        this.roleType = roleType;
    }

      @Override
    public String toString() {
        return "Roles{" + "id=" + id + ", roleType='" + roleType + '}';
    }
}