package com.sda.latnikovd.springbootapp.modules.accidents;

import com.sda.latnikovd.springbootapp.modules.accidents.Accidents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccidentsRepository extends JpaRepository<Accidents, Long>{
    Optional<Accidents> findById(long id);
}
