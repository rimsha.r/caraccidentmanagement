package com.sda.latnikovd.springbootapp.modules.accidents;

import com.sda.latnikovd.springbootapp.modules.users.User;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AccidentsService {

    @Autowired
    private AccidentsRepository accidentsRepository;

    @Transactional(readOnly = true)
    public List<Accidents> findAll() {
        return accidentsRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Accidents> findById(Long accidentId){
        return accidentsRepository.findById(accidentId);
    }

    @Transactional(rollbackFor = Exception.class)
    public Accidents save(final Accidents accident) {

        Long accidentsId = accident.getId();
        Validate.notNull(accident.getTime(), "time is blank for Accident '%s'", accident);
        Validate.notNull(accident.getLocationId(), "locationId is blank for Accident '%s'", accident);

        Validate.isTrue(accident.getId() == null, "ID cannot be defined in body!");

        return accidentsRepository.save(accident);
    }

}
