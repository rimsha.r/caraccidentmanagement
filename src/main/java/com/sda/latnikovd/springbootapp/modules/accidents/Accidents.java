package com.sda.latnikovd.springbootapp.modules.accidents;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "accidents")
public class Accidents {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private Date time;

    @Column
    private Long locationsId;

    @Column
    private Boolean submitted;

    public Accidents() {
    }

    public Accidents(Date time, Long locationId, Boolean submitted) {
        this.time = time;
        this.locationsId = locationId;
        this.submitted = submitted;
    }

    public Long getId() {
        return id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Long getLocationId() {
        return locationsId;
    }

    public void setLocationId(Long locationId) {
        this.locationsId = locationId;
    }

    public Boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(Boolean submitted) {
        this.submitted = submitted;
    }

    @Override
    public String toString() {
        return "Accidents{" +
                "id=" + id +
                ", time=" + time +
                ", locationId=" + locationsId +
                ", submitted=" + submitted +
                '}';
    }

}
