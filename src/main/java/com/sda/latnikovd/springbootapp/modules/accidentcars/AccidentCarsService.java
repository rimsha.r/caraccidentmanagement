package com.sda.latnikovd.springbootapp.modules.accidentcars;

import com.sda.latnikovd.springbootapp.modules.accidents.Accidents;
import com.sda.latnikovd.springbootapp.modules.accidents.AccidentsRepository;
import com.sda.latnikovd.springbootapp.modules.accidents.AccidentsService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccidentCarsService {

    @Autowired
    private AccidentCarsRepository accidentCarsRepository;

    @Autowired
    private AccidentsRepository accidentsRepository;

    @Transactional(readOnly = true)
    public List<AccidentCars> findAll() {
        return accidentCarsRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<AccidentCars> findAllByCarId(final long carId) {
        return accidentCarsRepository.findAllByCarId(carId);
    }

    @Transactional(readOnly = true)
    public List<AccidentCars> findAllByAccidentId(final long accidentId) {
        return accidentCarsRepository.findAllByAccidentId(accidentId);
    }

    @Transactional(rollbackFor = Exception.class)
    public AccidentCars save(final AccidentCars accidentCars) {

        Long accidentCarsId = accidentCars.getId();
        Validate.notNull(accidentCars.getAccidentId(), "accidentId is blank for accidentCars '%s'", accidentCars);
        Validate.notNull(accidentCars.getCarId(), "carId is blank for accidentCars '%s'", accidentCars);
        Validate.notNull(accidentCars.getDriverId(), "driverId is blank for accidentCars '%s'", accidentCars);

        AccidentCars foundAccidentCar = accidentCarsRepository.findByAccidentIdAndCarId(accidentCars.getAccidentId(), accidentCars.getCarId()).orElse(null);
        Validate.notNull(foundAccidentCar,"Car already added to this accident!");

        Validate.isTrue(accidentCars.getId() == null, "ID cannot be defined in body!");

        return accidentCarsRepository.save(accidentCars);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(long accidentId, long carId) {

        AccidentCars accidentCar = accidentCarsRepository.findByAccidentIdAndCarId(accidentId, carId).orElseThrow(()
                -> new RuntimeException("No such car found for this accident!"));

        Accidents foundAccident = accidentsRepository.findById(accidentId).orElse(null);
        Validate.notNull(foundAccident, "No such accident found!");

        Validate.isTrue(!foundAccident.isSubmitted(), "Accident is already submitted!");

        accidentCarsRepository.deleteById(accidentCar.getId());
    }

    @Transactional(rollbackFor = Exception.class)
    public void approve(long accidentCarId) {
        AccidentCars accidentCars = accidentCarsRepository.findById(accidentCarId).orElse(null);
        Validate.notNull(accidentCars, "Accident car with %s is empty, exiting", accidentCarId);
        accidentCars.setApproved(true);

        accidentCarsRepository.save(accidentCars);
        Long accidentId = accidentCars.getAccidentId();
        List <AccidentCars> accidentCarList = accidentCarsRepository.findAllByAccidentId(accidentId);

        for (AccidentCars accidentCar : accidentCarList) {
            if (!accidentCar.isApproved()) {
                // should log that all car drivers haven't approved of an accident
                // can't submit
                return;
            }
        }

        Accidents thisAccident = accidentsRepository.findById(accidentId.longValue()).orElseThrow(() -> new RuntimeException("No such accident found!"));
        thisAccident.setSubmitted(true);

        accidentsRepository.save(thisAccident);
    }
}

