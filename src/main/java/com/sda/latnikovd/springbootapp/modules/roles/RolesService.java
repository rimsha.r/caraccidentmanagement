package com.sda.latnikovd.springbootapp.modules.roles;

import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

// here we will put all the logic for authors model
@Service
public class RolesService {

    @Autowired
    private RolesRepository rolesRepository;

    @Transactional(readOnly = true)
    public List<Roles> findAll() {
        return rolesRepository.findAll();
    }

}
