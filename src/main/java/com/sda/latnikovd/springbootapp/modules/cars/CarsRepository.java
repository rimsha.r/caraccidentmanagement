package com.sda.latnikovd.springbootapp.modules.cars;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public
interface CarsRepository extends JpaRepository<Cars, Long> {
}
