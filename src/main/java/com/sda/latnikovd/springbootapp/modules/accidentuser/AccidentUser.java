package com.sda.latnikovd.springbootapp.modules.accidentuser;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "accident_user")
public class AccidentUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private Long userId;

    @Column
    private Long roleId;

    @Column
    private Long accidentId;

    public AccidentUser() {
    }

    public AccidentUser(Long userId, Long roleId, Long accidentId) {
        this.userId = userId;
        this.roleId = roleId;
        this.accidentId = accidentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getAccidentId() {
        return accidentId;
    }

    public void setAccidentId(Long accidentId) {
        this.accidentId = accidentId;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Accident_user{" +
                "user_id=" + userId +
                ", role_id='" + roleId + '\'' +
                ", accident_id='" + accidentId + '\'' +
                '}';
    }
}