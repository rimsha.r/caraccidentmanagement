package com.sda.latnikovd.springbootapp.modules.locations;

import com.sda.latnikovd.springbootapp.modules.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
interface LocationRepository extends JpaRepository<Location, Long> {

}