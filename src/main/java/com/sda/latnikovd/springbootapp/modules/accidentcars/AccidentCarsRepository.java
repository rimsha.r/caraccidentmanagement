package com.sda.latnikovd.springbootapp.modules.accidentcars;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
interface AccidentCarsRepository extends JpaRepository<AccidentCars, Long> {

    List<AccidentCars> findAllByCarId(long carId);
    List<AccidentCars> findAllByAccidentId(long accidentId);

    Optional<AccidentCars> findByAccidentIdAndCarId(long accidentId, long carsId);
}
