package com.sda.latnikovd.springbootapp.modules.cars;

import com.sda.latnikovd.springbootapp.modules.users.User;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CarsService {

    @Autowired
    private CarsRepository carsRepository;

    @Transactional(readOnly = true)
    public List<Cars> findAll() {
        return carsRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Cars save(final Cars cars) {

        Validate.notBlank(cars.getPlateNr(), "plateNr is blank for cars '%s'", cars);
        Validate.notBlank(cars.getCarBrand(), "carBrand is blank for cars '%s'", cars);

        Validate.isTrue(cars.getId() == null, "ID cannot be defined in body!");

        return carsRepository.save(cars);
    }
}
