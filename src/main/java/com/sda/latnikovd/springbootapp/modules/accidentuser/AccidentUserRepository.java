package com.sda.latnikovd.springbootapp.modules.accidentuser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

// please note that repository class is package private to minimize it's visibility
// only services should use corresponding repository classes!
@Repository
interface AccidentUserRepository extends JpaRepository<AccidentUser, Long> {

    List<AccidentUser> findAllByUserId(long userId);
    List<AccidentUser> findAllByAccidentId(long accidentId);

    Optional<AccidentUser> findByAccidentIdAndUserId(long accidentId, long userId);
}