package com.sda.latnikovd.springbootapp.modules.cars;

import javax.persistence.*;


@Entity
@Table(name = "cars")
public class Cars {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String plateNr;

    @Column
    private String carBrand;

    public Cars() {
    }

    public Cars(Long id, String plateNr, String carBrand) {
        this.id = id;
        this.plateNr = plateNr;
        this.carBrand = carBrand;
    }

    public Long getId() {
        return id;
    }

    public String getPlateNr() {
        return plateNr;
    }

    public void setPlateNr(String plateNr) {
        this.plateNr = plateNr;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "id=" + id +
                ", plateNr='" + plateNr + '\'' +
                ", carBrand='" + carBrand + '\'' +
                '}';
    }
}