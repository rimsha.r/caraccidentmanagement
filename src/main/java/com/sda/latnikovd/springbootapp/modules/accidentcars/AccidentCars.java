package com.sda.latnikovd.springbootapp.modules.accidentcars;

import javax.persistence.*;

@Entity
@Table(name = "accident_car")
public class AccidentCars {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column                  //check if boolean works together with BIT! If not, change BIT to TINYINT ->>> resources.migrations.006-accident-car.sql
    private Boolean approved;

    @Column
    private Long carId;

    @Column
    private Long accidentId;

    @Column
    private Long driverId;

    public AccidentCars() {
    }

    public AccidentCars(Boolean approved, Long carId, Long accidentId, Long driverId) {
        this.approved = approved;
        this.carId = carId;
        this.accidentId = accidentId;
        this.driverId = driverId;
    }

    public Long getId() {
        return id;
    }

    public Boolean isApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Long getAccidentId() {
        return accidentId;
    }

    public void setAccidentId(Long accidentId) {
        this.accidentId = accidentId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    @Override
    public String toString() {
        return "AccidentCars{" +
                "id=" + id +
                ", approved=" + approved +
                ", car_id=" + carId +
                ", accident_id=" + accidentId +
                ", driver_id=" + driverId +
                '}';
    }
}