package com.sda.latnikovd.springbootapp.modules.users;

import java.util.List;
import java.util.Optional;

import com.sda.latnikovd.springbootapp.modules.accidents.Accidents;
import com.sda.latnikovd.springbootapp.modules.accidents.AccidentsRepository;
import com.sda.latnikovd.springbootapp.modules.accidentuser.AccidentUser;
import com.sda.latnikovd.springbootapp.modules.accidentuser.AccidentUserService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccidentUserService accidentUserService;

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public User save(final User user) {

        Long userId = user.getId();
        Validate.notBlank(user.getFirstName(), "firstName is blank for user '%s'", user);
        Validate.notBlank(user.getLastName(), "lastName is blank for user '%s'", user);

        Validate.isTrue(user.getId() == null, "User ID cannot be defined in body!");

        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public User update(final User user) {

        User original = userRepository.findById(user.getId()).orElse(null);
        Validate.notNull(original, "Can't find user with ID %s ", user.getId());
        Validate.isTrue(user.getEmail().equals(original.getEmail()), "Can't update email %s to %s!", user.getEmail(), original.getEmail());

        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(long userId) {

        User user = userRepository.findById(userId).orElse(null);

        // jādabū accidenti, kas piesaistīti šim useram
        // jāpārbauda vai accidentu skaits ir lielāks par 0, ja ir 0 var delete ja ir lielāks tad rollback

        List<AccidentUser> accidentUser = accidentUserService.findAllByUserId(userId);
        Validate.isTrue(accidentUser.isEmpty(), "User can't be deleted, User %s has accident!", userId);

//        User thisUser = userRepository.findById(user.getId().orElseThrow(() -> new RuntimeException()) ;
//        Validate.isTrue(!thisAccident.isSubmitted(), "Accident is already submitted!");

        userRepository.deleteById(userId);
    }
}
